#![cfg_attr(
    all(not(debug_assertions), target_os = "windows"),
    windows_subsystem = "windows"
)]

fn main() {
    tauri::Builder::default()
        .invoke_handler(tauri::generate_handler![
            test_ips,
            save_config,
            load_config,
            save_test_result
        ])
        .run(tauri::generate_context!())
        .expect("error while running tauri application");
}
use scanner_lib::{run_test, Config, SavedConfig, TestResult, DEFAULT_IP_RANGES};
#[tauri::command]
async fn test_ips(config: Config, text: String) -> TestResult {
    run_test(config, text).await
}

const CONFIG_FILE_NAME: &str = "config.json";
const RESULTS_FOLDER_NAME: &str = "scanner_results";

#[tauri::command]
async fn save_config(app_handle: tauri::AppHandle, saved_config: SavedConfig) -> Result<(), ()> {
    let mut path = app_handle.path_resolver().app_config_dir().ok_or(())?;
    tokio::fs::create_dir_all(&path).await.map_err(|_| ())?;
    path.push(CONFIG_FILE_NAME);
    let res = saved_config.save(&path).await;
    res.map_err(|_| ())
}

#[tauri::command]
async fn load_config(app_handle: tauri::AppHandle) -> SavedConfig {
    if let Some(mut path) = app_handle.path_resolver().app_config_dir() {
        path.push(CONFIG_FILE_NAME);
        if let Ok(saved_config) = SavedConfig::load_from_file(&path).await {
            return saved_config;
        }
    }
    // default config
    SavedConfig::new(
        Config {
            isp: "".to_string(),
            sni: "speed.cloudflare.com".to_string(),
            url: "https://speed.cloudflare.com/__up".to_string(),
            number_of_ip_to_test: 1000,
            number_of_concurrent_requests: 10,
            file_size_kb: 200,
            timeout_ms: 2000,
            repeat_count: 8,
            min_acceptable_try_success: 6,
            no_ws: true,
            download_mode: false,
        },
        DEFAULT_IP_RANGES.to_string(),
    )
}
#[tauri::command]
async fn save_test_result(test_result: TestResult) -> Result<(), ()> {
    let mut path = tauri::api::path::download_dir().ok_or(())?;
    path.push(RESULTS_FOLDER_NAME);
    tokio::fs::create_dir_all(&path).await.map_err(|_| ())?;
    path.push(format!(
        "{}_{}.json",
        test_result.config.isp,
        chrono::offset::Local::now()
            .format("%Y-%m-%d %H:%M:%S")
            .to_string()
    ));
    test_result.save(&path).await.map_err(|_| ())
}
