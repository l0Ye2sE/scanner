import { addMessages, init } from "svelte-i18n"

export const prerender = true
export const ssr = false
import en from '../langs/en.json'
import fa from '../langs/fa.json'
const defaultLocale = 'en'

addMessages('en', en);
addMessages('fa', fa);

init({
  fallbackLocale: defaultLocale,
  initialLocale: defaultLocale,
})
