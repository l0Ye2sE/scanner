import { writable } from 'svelte/store';
import type { TestResult } from '../ipc-functions';

export const testResults = writable<TestResult | null>(null);
