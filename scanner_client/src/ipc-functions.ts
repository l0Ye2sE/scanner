import { invoke } from '@tauri-apps/api/tauri';
export type SavedConfig = {
  config: Config, ip_ranges: string
}
export type Config = {
  isp: string;
  sni: string;
  url: string;
  number_of_ip_to_test: number;
  number_of_concurrent_requests: number;
  file_size_kb: number;
  timeout_ms: number;
  repeat_count: number;
  min_acceptable_try_success: number;
  no_ws: boolean;
  download_mode: boolean;
};
export type OutputIp = {
  ip: CdnIp,
  success_try_count: number,
  avg_speed_byte_pre_s: number,
}
export const LONDON = "London"
export const WORLD_WIDE = "WorldWide"
export type IpLocation = typeof LONDON | typeof WORLD_WIDE | null;
export type CdnIp = {
  location?: IpLocation,
  ip: string,
}
export type TestResult = {
  config: Config,
  result_ips_count: number,
  result_ips: OutputIp[],
  test_time_ms: number,
}
export async function testIps(config: Config, text: string) {
  return await invoke('test_ips', { config, text }) as TestResult
}
export async function loadConfig() {
  return await invoke('load_config') as SavedConfig
}
export async function saveConfig(config: Config, ip_ranges: string) {
  return await invoke('save_config', { savedConfig: { config, ip_ranges } })
}
export async function saveTestResult(testResult: TestResult) {
  return await invoke('save_test_result', { testResult })
}
