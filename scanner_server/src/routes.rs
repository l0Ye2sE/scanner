use warp::Filter;

use crate::handlers;

pub fn download_route(
) -> impl Filter<Extract = (impl warp::Reply,), Error = warp::Rejection> + Clone {
    warp::path("download")
        .and(warp::query::<handlers::DownloadQuery>())
        .and(warp::get())
        .and_then(handlers::download)
}

pub fn download_ws_route(
) -> impl Filter<Extract = (impl warp::Reply,), Error = warp::Rejection> + Clone {
    let ws_upload_route = warp::path("download-ws")
        .and(warp::query::<handlers::DownloadQuery>())
        .and(warp::ws())
        .map(|query: handlers::DownloadQuery, ws: warp::ws::Ws| {
            ws.on_upgrade(move |socket| handlers::download_ws(query, socket))
        });
    ws_upload_route
}
pub fn upload_route() -> impl Filter<Extract = (impl warp::Reply,), Error = warp::Rejection> + Clone
{
    warp::path("upload")
        .and(warp::post())
        .and(warp::multipart::form().max_length(5_000_000))
        .and_then(handlers::upload)
}

pub fn upload_ws_route(
) -> impl Filter<Extract = (impl warp::Reply,), Error = warp::Rejection> + Clone {
    warp::path("upload-ws")
        .and(warp::ws())
        .map(|ws: warp::ws::Ws| ws.on_upgrade(move |socket| handlers::upload_ws(socket)))
}
