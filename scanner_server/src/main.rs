use warp::Filter;

const DEFAULT_PORT: u16 = 80;

#[tokio::main]
async fn main() {
    let port = std::env::args()
        .collect::<Vec<_>>()
        .get(1)
        .map_or(DEFAULT_PORT, |s| s.parse().unwrap_or(DEFAULT_PORT));

    let router = scanner_server::upload_route()
        .or(scanner_server::upload_ws_route())
        .or(scanner_server::download_route())
        .or(scanner_server::download_ws_route())
        .recover(scanner_server::handle_rejection);

    println!("Server started at 0.0.0.0:{}", port);

    println!("/upload-ws websocket upload path");
    println!("/upload http(s) upload path");
    println!("/download http(s) download path");
    println!("/download-ws websocket download path");

    warp::serve(router).run(([0, 0, 0, 0], port)).await;
}
