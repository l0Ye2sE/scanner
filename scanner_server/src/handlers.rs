use bytes::BufMut;
use futures::TryStreamExt;
use futures_util::SinkExt;
use futures_util::StreamExt;
use std::convert::Infallible;
use warp::ws::WebSocket;
use warp::{
    multipart::{FormData, Part},
    Rejection, Reply,
};
#[derive(serde::Deserialize)]
pub struct DownloadQuery {
    bytes: usize,
}
pub async fn download(query: DownloadQuery) -> Result<impl warp::Reply, Infallible> {
    let response = warp::http::Response::builder()
        .header(warp::http::header::CONTENT_TYPE, "application/octet-stream")
        .body(vec![0u8; query.bytes])
        .unwrap();
    Ok(response)
}
pub async fn download_ws(query: DownloadQuery, ws: WebSocket) {
    let (mut user_ws_sink, _) = ws.split();
    let _ = user_ws_sink
        .send(warp::ws::Message::binary(vec![0u8; query.bytes]))
        .await;
}

pub async fn upload(form: FormData) -> Result<impl Reply, Rejection> {
    let parts: Vec<Part> = form.try_collect().await.map_err(|e| {
        eprintln!("form error: {}", e);
        warp::reject::reject()
    })?;

    for p in parts {
        if p.name() == "file" {
            let _ = p
                .stream()
                .try_fold(Vec::new(), |mut vec, data| {
                    vec.put(data);
                    async move { Ok(vec) }
                })
                .await
                .map_err(|e| {
                    eprintln!("reading file error: {}", e);
                    warp::reject::reject()
                })?;
            return Ok(warp::reply::reply());
        }
    }
    Err(warp::reject())
}
pub async fn upload_ws(ws: WebSocket) {
    let (_, mut user_ws_rx) = ws.split();
    while let Some(_) = user_ws_rx.next().await {}
}
