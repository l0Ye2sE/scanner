use rand::Fill;
use serde::{Deserialize, Serialize};
use std::io;
use std::path::PathBuf;
use std::sync::Arc;
use tokio::sync::Mutex;

use tokio::io::{AsyncReadExt, AsyncWriteExt};

use crate::{http, ws};

#[derive(Serialize, Deserialize)]
pub struct SavedConfig {
    config: Config,
    ip_ranges: String,
}
impl SavedConfig {
    pub fn new(config: Config, ip_ranges: String) -> Self {
        Self { config, ip_ranges }
    }
    pub async fn save(&self, path: &PathBuf) -> io::Result<()> {
        let mut file = tokio::fs::File::create(path).await?;
        file.write(&serde_json::to_vec(self)?).await?;
        Ok(())
    }
    pub async fn load_from_file(path: &PathBuf) -> io::Result<Self> {
        let mut file = tokio::fs::File::open(path).await?;
        let mut s = String::new();
        file.read_to_string(&mut s).await?;
        let saved_config: SavedConfig = serde_json::from_str(&s).map_err(|e| {
            io::Error::new(
                io::ErrorKind::InvalidData,
                format!("Cannot parse json : {e}"),
            )
        })?;
        Ok(saved_config)
    }
}

#[derive(Serialize, Deserialize)]
pub struct Config {
    pub isp: String,
    pub sni: String,
    pub url: String,
    pub number_of_ip_to_test: usize,
    pub number_of_concurrent_requests: u64,
    pub file_size_kb: u64,
    pub timeout_ms: u64,
    pub repeat_count: u64,
    pub min_acceptable_try_success: u64,
    pub no_ws: bool,
    pub download_mode: bool,
}
#[derive(Serialize, Deserialize)]
pub struct TestResult {
    pub config: Config,
    pub result_ips_count: u64,
    pub result_ips: Vec<crate::OutputIp>,
    pub test_time_ms: u64,
}
impl TestResult {
    pub async fn save(&self, path: &PathBuf) -> std::io::Result<()> {
        let mut file = tokio::fs::File::create(path).await?;
        file.write(&serde_json::to_vec(self)?).await?;
        Ok(())
    }
}
pub async fn run_test(config: Config, unparsed_text: String) -> TestResult {
    let file_size_in_bytes = config.file_size_kb * 1024;
    let start_time = tokio::time::Instant::now();
    let ips = crate::random_ordered_ips_from_string(&unparsed_text);
    let tls_connector = crate::tls::make_tls_connector();
    let url_str = if config.download_mode {
        //trim the end slash if exists in client
        format!("{}?bytes={}", config.url, file_size_in_bytes)
    } else {
        config.url.clone()
    };
    let url: hyper::Uri = url_str.parse().unwrap();
    let body_bytes = crate::http::make_file_data(file_size_in_bytes).unwrap();
    let sni = crate::tls::make_server_name(&config.sni);

    let ws_url: url::Url = config.url.parse().unwrap();

    let a: Vec<Vec<_>> = ips[..config.number_of_ip_to_test.min(ips.len())]
        .chunks(
            (config.number_of_ip_to_test / config.number_of_concurrent_requests as usize).max(1),
        )
        .map(|arr| arr.to_vec())
        .collect();
    let res = Arc::new(Mutex::new(Vec::new()));
    let mut handles = Vec::new();
    for ip_list in a {
        handles.push(tokio::spawn({
            let res = res.clone();
            let tls_connector = tls_connector.clone();
            let url = url.clone();
            let body_bytes = body_bytes.clone();
            let sni = sni.clone();

            let ws_url = ws_url.clone();
            let mut ws_upload_buf = vec![1u8; file_size_in_bytes as usize];
            let mut rng = rand::thread_rng();
            ws_upload_buf.try_fill(&mut rng).unwrap();
            async move {
                for ip in &ip_list {
                    let mut count = 0;
                    let mut total = 0;
                    for _ in 1..=config.repeat_count {
                        let scan_fn = async {
                            match (config.no_ws, config.download_mode) {
                                (false, false) => {
                                    ws::test_ip(
                                        &tls_connector,
                                        ip.ip.clone(),
                                        &sni,
                                        &ws_url,
                                        ws::Mode::Upload(ws_upload_buf.as_ref()),
                                        config.timeout_ms,
                                        None,
                                    )
                                    .await
                                }
                                (false, true) => {
                                    ws::test_ip(
                                        &tls_connector,
                                        ip.ip.clone(),
                                        &sni,
                                        &ws_url,
                                        ws::Mode::Download,
                                        config.timeout_ms,
                                        None,
                                    )
                                    .await
                                }
                                (true, false) => {
                                    http::test_ip(
                                        &tls_connector,
                                        ip.ip.clone(),
                                        &url,
                                        &sni,
                                        http::Mode::Upload(body_bytes.clone().into()),
                                        config.timeout_ms,
                                        None,
                                    )
                                    .await
                                }
                                (true, true) => {
                                    http::test_ip(
                                        &tls_connector,
                                        ip.ip.clone(),
                                        &url,
                                        &sni,
                                        http::Mode::Download,
                                        config.timeout_ms,
                                        None,
                                    )
                                    .await
                                }
                            }
                        };
                        if let Ok(t) = scan_fn.await {
                            count += 1;
                            total += file_size_in_bytes * 1000 / t;
                        }
                    }
                    if count >= config.min_acceptable_try_success {
                        res.lock().await.push(crate::OutputIp {
                            ip: ip.clone(),
                            success_try_count: count,
                            avg_speed_byte_pre_s: total / count,
                        });
                        println!("{}  :   {}    :   {count}", ip.ip, total / count);
                    }
                }
            }
        }));
    }
    for j in handles {
        let _ = j.await;
    }
    let mut res = res.lock().await.clone();
    res.sort_by(|b, a| a.avg_speed_byte_pre_s.cmp(&b.avg_speed_byte_pre_s));
    println!("{}", res.len());
    println!("{}", start_time.elapsed().as_millis());
    TestResult {
        config,
        result_ips_count: res.len() as u64,
        result_ips: res,
        test_time_ms: start_time.elapsed().as_millis() as u64,
    }
}
