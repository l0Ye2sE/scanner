use std::net::Ipv4Addr;
use std::sync::Arc;
use tokio::net::TcpStream;
use tokio_rustls::rustls::{self, OwnedTrustAnchor};
use tokio_rustls::{client::TlsStream, TlsConnector};

pub fn make_tls_connector() -> TlsConnector {
    let mut root_cert_store = rustls::RootCertStore::empty();
    root_cert_store.add_server_trust_anchors(webpki_roots::TLS_SERVER_ROOTS.0.iter().map(|ta| {
        OwnedTrustAnchor::from_subject_spki_name_constraints(
            ta.subject,
            ta.spki,
            ta.name_constraints,
        )
    }));

    let config = rustls::ClientConfig::builder()
        .with_safe_defaults()
        .with_root_certificates(root_cert_store)
        .with_no_client_auth();
    TlsConnector::from(Arc::new(config))
}
pub fn make_server_name(domain: &str) -> rustls::ServerName {
    domain.try_into().unwrap()
}
pub async fn tls_handshake(
    tls_connector: &tokio_rustls::TlsConnector,
    ip: Ipv4Addr,
    sni: &rustls::ServerName,
    tls_handshake_timeout: Option<u64>,
) -> Result<TlsStream<TcpStream>, ()> {
    let (ws_stream, _ws_connect_time) = tokio::time::timeout(
        tokio::time::Duration::from_millis(tls_handshake_timeout.unwrap_or(450)),
        async {
            let t = tokio::time::Instant::now();
            let stream = TcpStream::connect(std::net::SocketAddr::new(ip.into(), 443))
                .await
                .map_err(|_| ())?;
            let stream = tls_connector
                .connect(sni.clone(), stream)
                .await
                .map_err(|_| ())?;
            Ok::<_, ()>((stream, t.elapsed().as_millis() as u64))
        },
    )
    .await
    .map_err(|_| ())?
    .map_err(|_| ())?;
    Ok(ws_stream)
}
