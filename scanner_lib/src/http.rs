use hyper::Request;
const BOUNDARY: &'static str = "------------------------ea3bbcf87c101592";

use std::io::Write;

pub enum Mode {
    Download,
    Upload(hyper::body::Body),
}

pub async fn test_ip(
    tls_connector: &tokio_rustls::TlsConnector,
    ip: std::net::Ipv4Addr,
    url: &hyper::Uri,
    sni: &tokio_rustls::rustls::ServerName,
    mode: Mode,
    timeout: u64,
    tls_timeout: Option<u64>,
) -> Result<u64, ()> {
    let tls_stream = crate::tls::tls_handshake(tls_connector, ip, sni, tls_timeout).await?;
    let res = tokio::time::timeout(tokio::time::Duration::from_millis(timeout), async {
        let (mut sender, conn) = hyper::client::conn::handshake(tls_stream)
            .await
            .map_err(|_| ())?;
        tokio::task::spawn(async move {
            if let Err(err) = conn.await {
                println!("Connection failed: {:?}", err);
            }
        });
        let req = match mode {
            Mode::Upload(body) => make_upload_request(url, body),
            Mode::Download => make_download_request(url),
        }?;
        let t = tokio::time::Instant::now();
        let a = sender.send_request(req).await.map_err(|_| ())?;
        Ok::<_, ()>((a, t.elapsed().as_millis()))
    })
    .await
    .map_err(|_| ())?
    .map_err(|_| ())?;
    if !res.0.status().is_success() {
        return Err(());
    }
    Ok(res.1 as u64)
}
fn make_download_request(url: &hyper::Uri) -> Result<Request<hyper::body::Body>, ()> {
    let authority = url.authority().unwrap().clone();
    Request::builder()
        .method(hyper::Method::GET)
        .uri(url)
        .header(hyper::header::HOST, authority.as_str())
        .body(hyper::Body::empty())
        .map_err(|_| ())
}
pub fn make_file_data(len: u64) -> std::io::Result<Vec<u8>> {
    let mut data = Vec::new();
    write!(data, "--{}\r\n", BOUNDARY)?;
    write!(
        data,
        r#"Content-Disposition: form-data; name="file"; filename="random.jpg"\r\n"#
    )?;
    write!(data, "Content-Type: image/jpeg\r\n")?;
    write!(data, "\r\n")?;

    data.extend_from_slice(&vec![1u8; len as usize]);
    write!(data, "\r\n")?;
    write!(data, "--{}--\r\n", BOUNDARY)?;

    Ok(data)
}
fn make_upload_request(
    url: &hyper::Uri,
    body: hyper::body::Body,
) -> Result<Request<hyper::body::Body>, ()> {
    let authority = url.authority().unwrap().clone();
    Request::builder()
        .method(hyper::Method::POST)
        .uri(url)
        .header(hyper::header::HOST, authority.as_str())
        .header(
            hyper::header::CONTENT_TYPE,
            format!("multipart/form-data; boundary={}", BOUNDARY),
        )
        .body(body)
        .map_err(|_| ())
}
