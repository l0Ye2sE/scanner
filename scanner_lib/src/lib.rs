use rand::prelude::*;
mod http;
use serde::{Deserialize, Serialize};
mod default_ip_ranges;
mod run;
mod tls;
mod ws;

pub use default_ip_ranges::IP_RANGES as DEFAULT_IP_RANGES;
pub use run::{run_test, Config, SavedConfig, TestResult};

use std::net::Ipv4Addr;
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct OutputIp {
    pub ip: CdnIp,
    pub success_try_count: u64,
    pub avg_speed_byte_pre_s: u64,
}
#[derive(Clone, Debug, Serialize, Deserialize)]
pub enum IpLocation {
    London,
    WorldWide,
}
fn parse_int(s: &str) -> u32 {
    s.parse().unwrap()
}
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct CdnIp {
    pub location: Option<IpLocation>,
    pub ip: Ipv4Addr,
}
pub fn cidr_to_ip_array(cidr: &str, cdn_location: Option<IpLocation>) -> Vec<CdnIp> {
    let parts = cidr.split("/").collect::<Vec<&str>>();
    let ip = parts[0];
    let mask: u8 = parts[1].parse().unwrap();
    let ip_parts = ip.split(".").collect::<Vec<&str>>();
    let start = (parse_int(ip_parts[0]) << 24)
        | (parse_int(ip_parts[1]) << 16)
        | (parse_int(ip_parts[2]) << 8)
        | parse_int(ip_parts[3]);
    let end = start | (0xffffffff >> mask);

    let mut ips = vec![];
    for i in start..=end {
        ips.push(CdnIp {
            ip: i.into(),
            location: cdn_location.clone(),
        });
    }
    return ips;
}
///each line should contain ip/range like 1.2.3.4/24
pub fn random_ordered_ips_from_string(s: &str) -> Vec<CdnIp> {
    let mut ips = s
        .lines()
        .map(|s| s.trim())
        .filter(|s| s.len() != 0)
        .map(|ip_range| cidr_to_ip_array(ip_range, None))
        .collect::<Vec<_>>()
        .concat();
    let mut rng = rand::thread_rng();
    ips.shuffle(&mut rng);
    ips
}
