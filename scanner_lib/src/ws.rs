use std::net::Ipv4Addr;

use futures_util::{SinkExt, StreamExt};
use tokio::net::TcpStream;
use tokio_rustls::client::TlsStream;
use tokio_rustls::rustls;
use tokio_tungstenite::WebSocketStream;
use tokio_tungstenite::{client_async, tungstenite::protocol::Message};
pub enum Mode<'buf> {
    Upload(&'buf [u8]),
    Download,
}
pub async fn test_ip(
    tls_connector: &tokio_rustls::TlsConnector,
    ip: Ipv4Addr,
    sni: &rustls::ServerName,
    url: &url::Url,
    mode: Mode<'_>,
    timeout: u64,
    _handshake_timeout: Option<u64>,
) -> Result<u64, ()> {
    let ws_stream = ws_handshake(tls_connector, ip, sni, url, None, None).await?;
    let res = tokio::time::timeout(tokio::time::Duration::from_millis(timeout), async {
        let (mut sink, mut stream) = ws_stream.split();
        let t = tokio::time::Instant::now();
        match mode {
            Mode::Upload(upload_buf) => {
                sink.send(Message::binary(upload_buf))
                    .await
                    .map_err(|_| ())?;
            }
            Mode::Download => {
                stream.next().await.ok_or(())?.map_err(|_| ())?;
            }
        };
        let elapsed = t.elapsed().as_millis();
        if elapsed == 0 {
            println!("Unknown origin error : elapsed time is less than 1 milli second");
            return Err(());
        }
        Ok::<_, ()>(elapsed)
    })
    .await
    .map_err(|_| ())?
    .map_err(|_| ())?;
    Ok(res as u64)
}
async fn ws_handshake(
    tls_connector: &tokio_rustls::TlsConnector,
    ip: Ipv4Addr,
    sni: &rustls::ServerName,
    url: &url::Url,
    tls_handshake_timeout: Option<u64>,
    ws_handshake_timeout: Option<u64>,
) -> Result<WebSocketStream<TlsStream<TcpStream>>, ()> {
    let tls_stream =
        crate::tls::tls_handshake(tls_connector, ip, sni, tls_handshake_timeout).await?;
    let (ws_stream, _ws_connect_time) = tokio::time::timeout(
        tokio::time::Duration::from_millis(ws_handshake_timeout.unwrap_or(400)),
        async {
            let t = tokio::time::Instant::now();
            let (ws_stream, resp) = client_async(url, tls_stream).await.map_err(|_| ())?;
            if resp.status().as_u16() != 101 {
                println!("status err {:?}", resp.status());
                return Err(());
            }
            Ok::<_, ()>((ws_stream, t.elapsed().as_millis() as u64))
        },
    )
    .await
    .map_err(|_| ())?
    .map_err(|_| ())?;
    Ok(ws_stream)
}
