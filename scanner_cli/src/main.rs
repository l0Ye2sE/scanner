use argh::FromArgs;
use tokio::io::AsyncReadExt;

#[derive(FromArgs)]
/// test cloudflare IPs
/// it also needs a server and domain in cloudflare to work with
struct Config {
    /// name of isp used as a prefix for result file name
    #[argh(option)]
    pub isp: String,
    /// sni that used in tls handshake like www.example.com
    #[argh(option)]
    pub sni: String,
    ///websocket over tls url to upload like wss://www.example.com/upload-ws or to download like
    ///wss://www.example.com/download
    ///https url used to upload to like https://www.example.com/upload or to download like
    ///https://www.example.com/download-ws
    #[argh(option, short = 'u')]
    pub url: String,
    /// how many random ip should get tested
    #[argh(option, short = 'n', default = "1000")]
    pub number_of_ip_to_test: usize,
    /// how many ips should put in test in same time? it can also be one more then this number
    #[argh(option, short = 'c', default = "10")]
    pub number_of_concurrent_requests: u64,
    /// in websocket upload mode size of upload file should at least be 80KB
    #[argh(option, short = 's', default = "80")]
    pub file_size_kb: u64,
    /// timeout in milliseconds
    #[argh(option, short = 't', default = "2000")]
    pub timeout_ms: u64,
    /// how many times repeat testing each ip
    #[argh(option, short = 'r', default = "8")]
    pub repeat_count: u64,
    /// minimum acceptable number of tries each ip should have in order to be included in results
    #[argh(option, short = 'm', default = "6")]
    pub min_acceptable_try_success: u64,
    /// does not use websocket for upload test
    #[argh(switch)]
    pub no_ws: bool,
    ///switch to download mode instead of upload
    #[argh(switch)]
    pub download_mode: bool,
}

#[tokio::main]
async fn main() {
    let config: Config = argh::from_env();
    let mut ips_text = String::new();
    let mut file = tokio::fs::File::open("./ips.txt").await.unwrap();
    file.read_to_string(&mut ips_text).await.unwrap();

    let config = scanner_lib::Config {
        isp: config.isp,
        sni: config.sni,
        url: config.url,
        number_of_ip_to_test: config.number_of_ip_to_test,
        number_of_concurrent_requests: config.number_of_concurrent_requests,
        file_size_kb: config.file_size_kb,
        timeout_ms: config.timeout_ms,
        repeat_count: config.repeat_count,
        min_acceptable_try_success: config.min_acceptable_try_success,
        no_ws: config.no_ws,
        download_mode: config.download_mode,
    };
    let test_result = scanner_lib::run_test(config, ips_text).await;

    let mut path = std::path::PathBuf::from(".");
    path.push("results");
    tokio::fs::create_dir_all(&path).await.unwrap();
    path.push(format!(
        "{}_{}.json",
        test_result.config.isp,
        chrono::offset::Local::now()
            .format("%Y-%m-%d %H:%M:%S")
            .to_string()
    ));
    test_result.save(&path).await.unwrap();
}
