> before using this repo note that i am not expert so maybe there are some or many problems
> i appreciate your help for reporting this problems
# CDN random IP scanner client and server
> it's based on upload
## credits
for making this i read/used some code of [Vahid Farid](https://github.com/vfarid/cf-ip-scanner) and [Morteza Bashsiz](https://github.com/MortezaBashsiz/CFScanner) also used the ip ranges of [Vahid Farid's python scanner](https://github.com/vfarid/cf-ip-scanner-py)
## client 
a GUI app using tauri and sveltekit
> i know the app is ugly
### linux
if you use ubuntu
```bash
wget https://codeberg.org/attachments/b0d3b7e0-3e9a-43fb-ad1d-5510b8ba6fb7
sudo dpkg -i scanner_client.deb
```
also you can use .AppImage file
```bash
#download size is bigger then release files
wget https://codeberg.org/attachments/c19a3257-5002-479d-bd1e-a42ca4bba130
chmod a+x scanner_client.AppImage
./scanner_client.AppImage
```
if the last command gives you error check [this](https://docs.appimage.org/user-guide/troubleshooting/fuse.html#the-appimage-tells-me-it-needs-fuse-to-run)
### windows
download [this file for 64bit windows](https://codeberg.org/attachments/7e652069-f8c4-4cb4-9e45-f3d5964631b0) and install it
### mac
you need to compile it yourself but i will give some links and steps that may help you
> I didn't compile the app in mac so i know this steps can be hard to follow or have issue **maybe it's better to install linux with virtualbox**

1. [setup tauri](https://tauri.app/v1/guides/getting-started/prerequisites/#setting-up-macos)
2. install nodejs
3. go to scanner_cleint directory
4. install packages
```bash
npm install
```
5. build the app

```bash
npm run tauri build
```
6. i guess it should give you bundle file address in the end

## server
> client can be used with speed.cloudflare.com and without a server but the results is not as good as having your own server with websocket mode, so i won't suggest it

simple warp (rust http server) server that has two routes
- /upload for using in https (no websocket) mode in client
- /upload-ws for using with websocket
> i think websocket mode is better
### How to use
- Cloudflare
  1. activate websocket
  2. in Cloudflare panel set TLS to flexible mode (maybe you also need to generate a cert in cloudflare)

  3. set proxy mode for your sub domain

- VPS server
  1. download the binary
  ```bash
  wget https://codeberg.org/attachments/aa072149-f2a5-40e5-99d6-156d5498507e
  ```
  2. change permission of the file
  ```bash
  chmod +x scanner_server
  ```
  3. run the binary with root user or sudo 
  ```bash
  sudo ./scanner_server
  ```
  > it uses port 80 so you need to run with sudo
## known bugs
- in windows save resutls button is not working (there is no reason user want to use this button when there is generate configs)
